/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 15:30:49 by jbailhac          #+#    #+#             */
/*   Updated: 2014/11/08 18:56:00 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	char		*pdst;
	const char	*psrc;

	pdst = dest;
	psrc = src;
	if (dest > src)
	{
		pdst = pdst + n - 1;
		psrc = psrc + n - 1;
		while (n--)
		{
			*pdst-- = *psrc--;
		}
	}
	else
	{
		while (n--)
		{
			*pdst++ = *psrc++;
		}
	}
	return (dest);
}
