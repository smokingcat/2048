/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strclr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 12:50:43 by jbailhac          #+#    #+#             */
/*   Updated: 2014/11/08 12:56:34 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void ft_strclr(char *s)
{
	int len;
	int i;

	i = 0;
	if (s != NULL)
	{
		len = ft_strlen(s);
		while (i < len)
		{
			s[i] = '\0';
			i++;
		}
	}
}
