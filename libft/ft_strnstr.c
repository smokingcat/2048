/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 13:31:56 by jbailhac          #+#    #+#             */
/*   Updated: 2014/11/08 18:52:52 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t	i;
	size_t	j;
	char	*tmp;

	i = 0;
	j = 0;
	tmp = (char *)s1;
	while (s1[i] && s2[j] && i < n)
	{
		if (s1[i] == s2[j])
			j++;
		else if (s1[i] != s2[j])
			j = 0;
		i++;
		tmp++;
	}
	if (j == ft_strlen(s2))
		return ((char *)s1 + i - j);
	return (0);
}
