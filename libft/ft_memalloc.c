/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 11:48:56 by jbailhac          #+#    #+#             */
/*   Updated: 2014/11/25 14:00:00 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	*ft_memalloc(size_t size)
{
	void	*zone;
	char	*tmp;
	size_t	i;

	i = 0;
	zone = (void *)malloc(size);
	if (!zone)
		return (NULL);
	tmp = zone;
	while (i < size)
	{
		tmp[i] = 0;
		i++;
	}
	return (zone);
}
