/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rush.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 10:50:22 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/01 20:41:43 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_RUSH_H
# define FT_RUSH_H

# include "../libft/libft.h"
# include <signal.h>
# include <ncurses.h>
# include <unistd.h>
# include <stdlib.h>
# include <menu.h>
# include <time.h>

typedef struct		s_game
{
	int				**tiles;
	int				won;
	int				lost;
	int				max_x;
	int				max_y;
	int				init;
	int				off_x;
	int				off_y;
	int				move;
	int				rotate_nb;
	int				moved;
}					t_game;

typedef struct		s_menu
{
	ITEM			**items;
	MENU			*menu;
	int				sel;
	char			**choices;
	int				max_x;
	int				max_y;
}					t_menu;

# define UP 2
# define DOWN 4
# define LEFT 8
# define RIGHT 16

enum				e_const
{
	WIN_VALUE = 2048
};

void				print_game(t_game game);
int					**init_tiles(void);
int					update_game(t_game *game);
t_game				*init_game(t_game *game);
int					merge_tiles(t_game *game, int dir);
int					win(t_game game);
int					is_full(t_game game);
int					check_lost(t_game game);
t_game				*rotate_tab(t_game *game);
int					ft_print_menu(void);

#endif
