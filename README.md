# 2048 #

A simple C implementation of the game [2048](http://gabrielecirulli.github.io/2048/) for the shell.

### Set up ###
```
git clone https://smokingcat@bitbucket.org/smokingcat/2048.git
cd game_2048
make
./game_2048
```
### What we learned ###
* Make powerful shell applications thanks to the NCurses API
* Tweaking git to collaborate seamlessly on the same files
### Preview ###
![2048_image](http://i.imgur.com/XwtNcYB.png?1)