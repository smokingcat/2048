/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   win.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 20:55:44 by vde-chab          #+#    #+#             */
/*   Updated: 2015/03/01 17:12:47 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_rush.h"

int		win(t_game game)
{
	int x;
	int y;

	x = 0;
	y = 0;
	if (game.won)
		return (0);
	while (y <= 3)
	{
		while (x <= 3)
		{
			if (game.tiles[x][y] == WIN_VALUE)
				return (1);
			x++;
		}
		x = 0;
		y++;
	}
	return (0);
}
