/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 10:52:54 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/01 19:33:01 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_rush.h"
#include <time.h>

static int		check_win(void)
{
	int row;
	int col;

	getmaxyx(stdscr, row, col);
	mvprintw(row / 2 - 1, (col - ft_strlen("*********************************\
	********")) / 2, "%s", "*****************************************");
	mvprintw(row / 2, (col - ft_strlen("*************** YOU WIN *************\
	****")) / 2, "%s", "*************** YOU WIN *****************");
	mvprintw(row / 2 + 1, (col - ft_strlen("*********************************\
	********")) / 2, "%s", "*****************************************");
	refresh();
	sleep(2);
	return (1);
}

static void		init_screen(void)
{
	initscr();
	noecho();
	timeout(0);
	curs_set(FALSE);
	keypad(stdscr, TRUE);
	nodelay(stdscr, TRUE);
}

static int		check_win_value(void)
{
	if (WIN_VALUE <= 1)
	{
		ft_putendl("WIN_VALUE is invalid.");
		return (-1);
	}
	if (WIN_VALUE & (WIN_VALUE - 1))
	{
		ft_putendl("WIN_VALUE is invalid.");
		return (-1);
	}
	return (0);
}

static void		spawn_init(t_game *game)
{
	int done;
	int rand_x;
	int rand_y;

	done = 0;
	while (done <= 1)
	{
		srand(time(NULL));
		rand_x = rand() % 4;
		rand_y = rand() % 4;
		if (!game->tiles[rand_x][rand_y])
		{
			game->tiles[rand_x][rand_y] = ((rand() % 1000) % 2) ? 2 : 4;
			done++;
		}
	}
}

int				main(void)
{
	t_game *game;

	if (check_win_value() == -1)
		return (0);
	init_screen();
	if (ft_print_menu() == -1)
		return (0);
	game = NULL;
	game = init_game(game);
	spawn_init(game);
	print_game(*game);
	while (1)
	{
		if (check_lost(*game))
			break ;
		if (update_game(game) == -1)
			break ;
		print_game(*game);
		if (win(*game) && game->won == 0)
			game->won = check_win();
	}
	refresh();
	endwin();
	return (0);
}
