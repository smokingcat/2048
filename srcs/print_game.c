/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_game.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 12:31:11 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/01 18:20:43 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_rush.h"

static int		get_pair_nb(int num)
{
	int pair_nb;
	int number;

	pair_nb = 0;
	number = num;
	while (number > 0)
	{
		number = number >> 1;
		pair_nb++;
	}
	return (pair_nb % 7);
}

static int		get_color(int num)
{
	int pair_nb;

	if (num >= 2)
		pair_nb = get_pair_nb(num);
	else
		return (0);
	if (pair_nb == 1)
		init_pair(1, COLOR_GREEN, COLOR_BLACK);
	else if (pair_nb == 2)
		init_pair(2, COLOR_BLUE, COLOR_BLACK);
	else if (pair_nb == 3)
		init_pair(3, COLOR_CYAN, COLOR_BLACK);
	else if (pair_nb == 4)
		init_pair(4, COLOR_RED, COLOR_BLACK);
	else if (pair_nb == 5)
		init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
	else if (pair_nb == 6)
		init_pair(6, COLOR_YELLOW, COLOR_BLACK);
	else if (pair_nb == 7)
		init_pair(7, COLOR_WHITE, COLOR_BLACK);
	return (pair_nb);
}

static void		print_cell(t_game game, int x, int y)
{
	WINDOW	*cell;
	int		tile_x;
	int		tile_y;
	char	*number;
	int		color;

	start_color();
	tile_x = game.max_x / 4;
	tile_y = game.max_y / 4;
	cell = newwin(tile_x, tile_y, (tile_x * x), (tile_y * y));
	wbkgd(cell, COLOR_PAIR(7));
	box(cell, 0, 0);
	if (game.tiles[x][y] != 0)
	{
		color = get_color(game.tiles[x][y]);
		wattron(cell, COLOR_PAIR (color));
		wbkgd(cell, COLOR_PAIR (color));
		number = ft_itoa(game.tiles[x][y]);
		mvwprintw(cell, tile_x / 2, tile_y / 2, number);
		ft_strdel(&number);
	}
	wrefresh(cell);
	delwin(cell);
}

void			print_game(t_game game)
{
	int		x;
	int		y;

	getmaxyx(stdscr, game.max_x, game.max_y);
	if (game.max_x < 16 || game.max_y < 16)
	{
		mvprintw(1, 1, "window is too small");
		refresh();
		sleep(2);
		return ;
	}
	x = 0;
	while (x < 4)
	{
		y = 0;
		while (y < 4)
		{
			print_cell(game, x, y);
			y++;
		}
		x++;
	}
}
