/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   update_game.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 12:32:18 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/01 19:16:05 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_rush.h"

static void		spawn_random(t_game *game)
{
	int done;
	int rand_x;
	int rand_y;

	done = 0;
	while (!done)
	{
		if (!is_full(*game))
		{
			rand_x = rand() % 4;
			rand_y = rand() % 4;
			if (!game->tiles[rand_x][rand_y])
			{
				game->tiles[rand_x][rand_y] = ((rand() % 1000) % 2) ? 2 : 4;
				done = 1;
			}
		}
		else
			break ;
	}
	return ;
}

int				update_game(t_game *game)
{
	int ch;

	while (!((ch = getch()) == ERR))
	{
		if (ch == 27)
			return (-1);
		if (ch == KEY_UP || ch == KEY_DOWN || ch == KEY_RIGHT || ch == KEY_LEFT)
		{
			if (merge_tiles(game, ch))
				spawn_random(game);
		}
	}
	return (0);
}
