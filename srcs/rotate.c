/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 16:34:19 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/01 17:46:05 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_rush.h"

static int		**free_tab(int **array)
{
	int i;

	i = 0;
	while (i <= 4)
	{
		free(array[i]);
		i++;
	}
	if (array)
	{
		free(array);
		array = NULL;
	}
	return (array);
}

t_game			*rotate_tab(t_game *game)
{
	int		**new;
	int		x;
	int		y;

	x = 0;
	new = init_tiles();
	while (x < 4)
	{
		y = 0;
		while (y < 4)
		{
			new[x][y] = game->tiles[3 - y][x];
			y++;
		}
		x++;
	}
	game->tiles = free_tab(game->tiles);
	game->tiles = new;
	return (game);
}
