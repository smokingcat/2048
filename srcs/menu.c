/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   menu.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 17:56:23 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/01 20:40:03 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_rush.h"

static t_menu		init_menu(void)
{
	t_menu menu;

	menu.choices = (char **)malloc(sizeof(char *) * 2);
	menu.choices[0] = ft_strdup("play");
	menu.choices[1] = ft_strdup("exit");
	menu.sel = 0;
	menu.items = (ITEM **)malloc(sizeof(ITEM *) * 3);
	menu.items[0] = new_item(menu.choices[0], " ");
	menu.items[1] = new_item(menu.choices[1], " ");
	menu.items[2] = NULL;
	menu.menu = new_menu((ITEM **)menu.items);
	return (menu);
}

static int			ft_free_menu(t_menu menu)
{
	int ret;

	ret = menu.sel;
	unpost_menu(menu.menu);
	free_menu(menu.menu);
	free_item(menu.items[0]);
	free_item(menu.items[1]);
	free_item(menu.items[2]);
	free(menu.items);
	ft_strdel(&(menu.choices[0]));
	ft_strdel(&(menu.choices[1]));
	free(menu.choices);
	return (ret);
}

static t_menu		refresh_menu(WINDOW *win, t_menu menu)
{
	getmaxyx(stdscr, menu.max_x, menu.max_y);
	mvwin(win, menu.max_x / 2, menu.max_y / 2);
	wrefresh(win);
	refresh();
	return (menu);
}

static t_menu		process_menu(WINDOW *win, t_menu menu, int c, int ret)
{
	while (1)
	{
		menu = refresh_menu(win, menu);
		c = getch();
		if (c == 27)
		{
			menu.sel = 1;
			return (menu);
		}
		if (c == '\n')
			break ;
		if (c == KEY_DOWN)
		{
			ret = menu_driver(menu.menu, REQ_DOWN_ITEM);
			if (ret == 0)
				menu.sel++;
		}
		if (c == KEY_UP)
		{
			ret = menu_driver(menu.menu, REQ_UP_ITEM);
			if (ret == 0)
				menu.sel++;
		}
	}
	return (menu);
}

int					ft_print_menu(void)
{
	t_menu	menu;
	WINDOW	*win;
	int		ret;

	ret = 0;
	menu = init_menu();
	getmaxyx(stdscr, menu.max_x, menu.max_y);
	win = newwin(2, 10, 0, 0);
	mvwin(win, menu.max_x / 2, menu.max_y / 2);
	set_menu_win(menu.menu, win);
	refresh();
	post_menu(menu.menu);
	wrefresh(win);
	menu = process_menu(win, menu, 0, 0);
	ret = ft_free_menu(menu);
	if (ret % 2 == 0)
		return (0);
	delwin(win);
	endwin();
	return (-1);
}
