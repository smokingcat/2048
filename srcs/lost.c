/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lost.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 12:01:28 by vde-chab          #+#    #+#             */
/*   Updated: 2015/03/01 17:05:59 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_rush.h"

static int		check_vertically(t_game game, int x, int y)
{
	while (x <= 3)
	{
		while (y <= 2)
		{
			if (game.tiles[y][x] == game.tiles[y + 1][x])
				return (0);
			y++;
		}
		y = 0;
		x++;
	}
	return (1);
}

static int		check_horizontally(t_game game, int x, int y)
{
	while (y <= 3)
	{
		while (x <= 2)
		{
			if (game.tiles[y][x] == game.tiles[y][x + 1])
				return (0);
			x++;
		}
		x = 0;
		y++;
	}
	return (1);
}

static int		no_move(t_game game)
{
	if (!check_horizontally(game, 0, 0) || !check_vertically(game, 0, 0))
		return (0);
	return (1);
}

int				check_lost(t_game game)
{
	int		row;
	int		col;
	char	*stars;
	char	*lost;

	lost = "***************** YOU LOST ****************";
	stars = "*******************************************";
	if (is_full(game) && no_move(game))
	{
		getmaxyx(stdscr, row, col);
		mvprintw(row / 2 - 1, (col - ft_strlen(stars)) / 2, "%s", stars);
		mvprintw(row / 2, (col - ft_strlen(lost)) / 2, "%s", lost);
		mvprintw(row / 2 + 1, (col - ft_strlen(stars)) / 2, "%s", stars);
		refresh();
		sleep(5);
		return (1);
	}
	return (0);
}
